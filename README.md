ScalaTest - BeforeAndAfter Doesn't Work
======================================

This is an example project to describe issues I ran into with ScalaTest's `BeforeAndAfterAll` trait. The full writeup can be found at [http://penguindreams.org/blog/scalatest-beforeandafterall-does-not-work/](http://penguindreams.org/blog/scalatest-beforeandafterall-does-not-work/)
