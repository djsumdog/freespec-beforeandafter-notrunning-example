package org.penguindreams

import org.scalatest.{BeforeAndAfterAll, FreeSpec, Matchers}

class NestedIn extends FreeSpec with Matchers with BeforeAndAfterAll {
  var example = false

  override def beforeAll() = {
    example = true
  }

  override def afterAll() = {
    example = false
  }

  "Some Test Set" - {
    "top in clause" in {
      example shouldBe true
      "nested in clause" in {
        example shouldBe true
      }
    }
  }
}
